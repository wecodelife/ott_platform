// To parse this JSON data, do
//
//     final loginUserRequest = loginUserRequestFromJson(jsonString);

import 'dart:convert';

LoginUserRequest loginUserRequestFromJson(String str) => LoginUserRequest.fromJson(json.decode(str));

String loginUserRequestToJson(LoginUserRequest data) => json.encode(data.toJson());

class LoginUserRequest {
    LoginUserRequest({
        this.status,
        this.token,
        this.userData,
    });

    int status;
    String token;
    UserData userData;

    factory LoginUserRequest.fromJson(Map<String, dynamic> json) => LoginUserRequest(
        status: json["status"],
        token: json["token"],
        userData: UserData.fromJson(json["user_data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "token": token,
        "user_data": userData.toJson(),
    };
}

class UserData {
    UserData({
        this.id,
        this.email,
        this.firstName,
        this.lastName,
        this.isVerified,
    });

    int id;
    String email;
    String firstName;
    String lastName;
    bool isVerified;

    factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        isVerified: json["is_verified"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "first_name": firstName,
        "last_name": lastName,
        "is_verified": isVerified,
    };
}
