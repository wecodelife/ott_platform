// To parse this JSON data, do
//
//     final registerUserRequest = registerUserRequestFromJson(jsonString);

import 'dart:convert';

RegisterUserRequest registerUserRequestFromJson(String str) =>
    RegisterUserRequest.fromJson(json.decode(str));

String registerUserRequestToJson(RegisterUserRequest data) =>
    json.encode(data.toJson());

class RegisterUserRequest {
  RegisterUserRequest({
    this.email,
    this.firstName,
    this.lastName,
    this.password,
    this.isVerified,
  });

  final String email;
  final String firstName;
  final String lastName;
  final String password;
  final bool isVerified;

  factory RegisterUserRequest.fromJson(Map<String, dynamic> json) =>
      RegisterUserRequest(
        email: json["email"] == null ? null : json["email"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        password: json["password"] == null ? null : json["password"],
        isVerified: json["is_verified"] == null ? null : json["is_verified"],
      );

  Map<String, dynamic> toJson() => {
        "email": email == null ? null : email,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "password": password == null ? null : password,
        "is_verified": isVerified == null ? null : isVerified,
      };
}
