import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class DropDownList extends StatefulWidget {
  List<String> dropDownList;
  String title;
  String dropDownValue;
  ValueChanged<String> onClicked;
  DropDownList(
      {this.title, this.dropDownList, this.dropDownValue, this.onClicked});
  @override
  _DropDownListState createState() => _DropDownListState();
}

String dropdownValue;

class _DropDownListState extends State<DropDownList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: DropdownButton<String>(
        underline: Divider(
          thickness: 1.0,
          color: Constants.kitGradients[24],
          //endIndent: screenWidth(context, dividedBy: 5.5),
          //indent:20,
        ),
        icon: Icon(
          Icons.arrow_drop_down,
          size: 30,
          color: Constants.kitGradients[29],
        ),
        iconSize: 20,
        dropdownColor: Constants.kitGradients[28],
        value:
            widget.dropDownValue == "" ? dropdownValue : widget.dropDownValue,
        style: TextStyle(fontSize: 16, color: Constants.kitGradients[29]),
        items: widget.dropDownList
            .map<DropdownMenuItem<String>>(
              (String value) => DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight:FontWeight.bold,
                    fontFamily: 'OpenSansRegular',
                    color: Constants.kitGradients[29],
                  ),
                ),
              ),
            )
            .toList(),
        onChanged: (selectedValue) {
          setState(() {
            widget.dropDownValue = selectedValue;
            print(widget.dropDownValue);
          });
          widget.onClicked(widget.dropDownValue);
        },
        hint: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              widget.title,
              style: TextStyle(fontSize: 18, color: Constants.kitGradients[27]),
            ),
          ],
        ),
      ),
    );
  }
}
