import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';

class HeadingTile extends StatefulWidget {
  final String heading;
  final Function onPressed;
  HeadingTile({this.heading, this.onPressed});
  @override
  _HeadingTileState createState() => _HeadingTileState();
}

class _HeadingTileState extends State<HeadingTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // padding:
      //     //EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 30)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(widget.heading,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: Constants.kitGradients[30],
              )),
          GestureDetector(
            onTap: widget.onPressed,
            child: Text(
              "See more",
              style: TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.w400,
                color: Constants.kitGradients[29],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
