import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HorizontalListTile extends StatefulWidget {
  final String imgUrl;
  final Function cardPressed;
  HorizontalListTile({this.imgUrl, this.cardPressed});
  @override
  _HorizontalListTileState createState() => _HorizontalListTileState();
}

class _HorizontalListTileState extends State<HorizontalListTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 3.5),
      // margin: EdgeInsets.symmetric(
      //   horizontal: screenHeight(context, dividedBy: 150),
      // ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: Constants.kitGradients[28],
      ),
      child: Column(
        children: [
          //MovieBottom
          GestureDetector(
            onTap: widget.cardPressed,
            child: ClipRRect(
             // borderRadius: BorderRadius.circular(6),
              child: CachedNetworkImage(
                width: screenWidth(context, dividedBy: 3.5),
                height: screenWidth(context, dividedBy: 3.5),
                // fit: BoxFit.cover,
                placeholder: (context, url) => Center(
                  heightFactor: 1,
                  widthFactor: 1,
                  child: SizedBox(
                    height: 16,
                    width: 16,
                    child: CircularProgressIndicator(
                      valueColor : AlwaysStoppedAnimation(Constants.kitGradients[29]),
                      strokeWidth: 2,
                    ),
                  ),
                ),
                imageUrl: widget.imgUrl,
                imageBuilder: (context, imageProvider) => Container(
                  width: screenWidth(context, dividedBy: 3.5),
                  height: screenWidth(context, dividedBy: 3.5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: Constants.kitGradients[28],
                    //shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.fill),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
