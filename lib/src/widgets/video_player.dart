import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoWidget extends StatefulWidget {
  final String url;
  final String title;
  final String movieType;
  final String description;
  final int ind;

  VideoWidget(
      {this.url, this.title, this.movieType, this.description, this.ind});
  @override
  _VideoWidgetState createState() => _VideoWidgetState();
}

bool visible = true;
List<String> videos = [
  "https://www.ibrahimabah.com/ibfilms/Harry.Potter-The.Ultimate.Collection.%20I%20-%20VIII%20.%202001-2011.1080p.Bluray.x264.anoXmous/Harry%20Potter%208/Harry.Potter.And.The.Deathly.Hallows.Part.2.2011.1080p.Bluray.x264.anoXmous_.mp4",
  "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
  "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4",
];
final List<String> titles = [
  "Batman",
  "Mortal Kombat",
  "RockStar",
];
final List<String> movieType = [
  "Action",
  "Action",
  "Drama",
];
final List<String> imgs = [
  "https://cdn.dribbble.com/users/3408570/screenshots/10746910/media/25b3567e37c225bad3dfb92133e1dc36.jpg?compress=1&resize=800x600",
  "https://www.joblo.com/assets/images/joblo/news/2021/03/mortal-kombat-2021-poster-group.jpg",
  "https://hdfreewallpaper.net/wp-content/uploads/2015/03/movies-wallpaper.jpg",
];
final List<String> rating = [
  "8.2",
  "7.4",
  "6.4",
];
final List<String> duration = [
  "2 hr 20 min",
  "1 hr 19 min",
  "2 hr 3 min",
];
final List<String> episodeSubtitles = [
  "Episode 1 Description",
  "Episode 2 Description",
  "Episode 3 Description",
  "Episode 4 Description",
  "Episode 5 Description",
  "Episode 6 Description",
];

class _VideoWidgetState extends State<VideoWidget> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.url);
    _initializeVideoPlayerFuture = _controller.initialize();
    _chewieController = ChewieController(
      videoPlayerController: _controller,
      looping: true,
      allowPlaybackSpeedChanging: false,
    );
    setState(() {});
    // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    _chewieController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Flexible(
        child: Column(children: [
          FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return _chewieController != null
                    ? AspectRatio(
                        aspectRatio: 16 / 9,
                        child: Stack(children: [
                          Column(children: [
                            Stack(
                              children: [
                                Container(
                                    height: screenHeight(context, dividedBy: 4.3),
                                    width: screenWidth(context, dividedBy: 1),
                                    child: Theme(
                                        data: Theme.of(context).copyWith(
                                          dialogBackgroundColor:
                                              Constants.kitGradients[28],
                                          accentColor: Constants.kitGradients[29],
                                          iconTheme: IconThemeData(
                                              color: Constants.kitGradients[29]),
                                        ),
                                        child:
                                            Chewie(controller: _chewieController))),
                                // Container(
                                //   height: screenHeight(context, dividedBy: 4.3),
                                //   width: screenWidth(context, dividedBy: 1),
                                //   child: Center(
                                //     child: _chewieController.isPlaying
                                //         ? AnimatedOpacity(
                                //             opacity: visible ? 1.0 : 0.0,
                                //             duration: Duration(milliseconds: 500),
                                //             child: Container(
                                //               decoration: BoxDecoration(
                                //                 color: Constants.kitGradients[30],
                                //                 //color:Colors.transparent,
                                //                 shape: BoxShape.circle,
                                //               ),
                                //               child: GestureDetector(
                                //                 child: Icon(Icons.pause_outlined,
                                //                     color:
                                //                         Constants.kitGradients[29],
                                //                     size: 60),
                                //                 onTap: () {
                                //                   setState(() {
                                //                     _chewieController.pause();
                                //                     visible = !visible;
                                //                   });
                                //                 },
                                //               ),
                                //             ),
                                //           )
                                //         : AnimatedOpacity(
                                //             opacity: visible ? 1.0 : 0.0,
                                //             duration: Duration(milliseconds: 500),
                                //             child: Container(
                                //               decoration: BoxDecoration(
                                //                 color: Constants.kitGradients[30],
                                //                 //color:Colors.transparent,
                                //                 shape: BoxShape.circle,
                                //               ),
                                //               child: GestureDetector(
                                //                 child: Icon(
                                //                     Icons.play_arrow_rounded,
                                //                     color:
                                //                         Constants.kitGradients[29],
                                //                     size: 60),
                                //                 onTap: () {
                                //                   setState(() {
                                //                     _chewieController.play();
                                //                     visible = !visible;
                                //                   });
                                //                 },
                                //               ),
                                //             ),
                                //           ),
                                //   ),
                                // ),
                              ],
                            )
                          ])
                        ]))
                    : Container();
              } else {
                return Container(
                  height: screenHeight(context, dividedBy: 4.3),
                  width: screenWidth(context, dividedBy: 1),
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation(Constants.kitGradients[32]),
                      strokeWidth: 2,
                    ),
                  ),
                );
              }
            },
          ),
          Container(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Row(
                children: [
                  Container(
                    child: Text(
                      widget.title,
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Constants.kitGradients[29],
                      ),
                    ),
                  ),
                  //Spacer(),
                  // Container(
                  //     child: GestureDetector(
                  //   onTap: () {
                  //     pushAndReplacement(
                  //         context,
                  //         MovieDetails(
                  //             movieTitle: titles[widget.ind],
                  //             subTitle: movieType[widget.ind],
                  //             imgUrl: imgs[widget.ind],
                  //             description:
                  //                 "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                  //             rating: rating[widget.ind],
                  //             time: duration[widget.ind],
                  //             isSeries: false,
                  //             url: videos[widget.ind]));
                  //   },
                  //   child: Row(
                  //     children: [
                  //       Icon(
                  //         Icons.play_circle_outline,
                  //         color: Constants.kitGradients[18],
                  //       ),
                  //       Text(
                  //         "Watch Now",
                  //         style: TextStyle(
                  //           fontSize: 12,
                  //           fontWeight: FontWeight.bold,
                  //           color: Constants.kitGradients[18],
                  //         ),
                  //       )
                  //     ],
                  //   ),
                  // )),
                ],
              ),
              Text(
                widget.movieType,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Constants.kitGradients[30],
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Text(
                widget.description,
                //textAlign: TextAlign.justify,
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  color: Constants.kitGradients[30],
                ),
              ),
            ],
          ))
        ]),
      ),
    );
  }
}
