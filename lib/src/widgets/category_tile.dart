import 'package:app_template/src/screens/trending_list_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/heading_tile.dart';
import 'package:app_template/src/widgets/market_place_card.dart';
import 'package:flutter/material.dart';

class CategoryListTile extends StatefulWidget {
  final List<String> itemtype;
  final List<String> images;
  final String categoryType;
  final Function seeMore;
  final Function itemPressed;
  final int currentIndex;
  CategoryListTile(
      {this.itemtype,
      this.images,
      this.categoryType,
      this.seeMore,
      this.itemPressed,
      this.currentIndex});

  @override
  _CategoryListTileState createState() => _CategoryListTileState();
}

class _CategoryListTileState extends State<CategoryListTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 1),
        margin: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 40),
          vertical: screenHeight(context, dividedBy: 50),
        ),
        padding: EdgeInsets.symmetric(
          //vertical: screenHeight(context, dividedBy: 50),
          horizontal: screenWidth(context, dividedBy: 60),
        ),
        child: Column(children: [
          HeadingTile(
            heading: widget.categoryType,
            onPressed: () {
              push(
                  context,
                  TrendingList(
                    pageTitle: widget.categoryType,
                    selection: false,
                  ));
            },
          ),
          SizedBox(height: screenHeight(context, dividedBy: 70)),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(
              //vertical: screenHeight(context, dividedBy: 50),
              horizontal: screenWidth(context, dividedBy: 60),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Constants.kitGradients[28],
            ),
            //color: Colors.grey,
            child: Column(
              children: [
                SizedBox(height: screenHeight(context, dividedBy: 50)),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 5),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: widget.images.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Row(children: [
                        MarketPlaceCard(
                          imgUrl: widget.images[index],
                          catType: widget.itemtype[index],
                          circle: false,
                          itemPressed: widget.itemPressed,
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 30),
                        )
                      ]);
                    },
                  ),
                ),
              ],
            ),
          )
        ]));
  }
}
