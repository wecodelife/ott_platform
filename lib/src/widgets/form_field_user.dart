import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFeildUserDetails extends StatefulWidget {
  final String labelText;
  final bool isPassword;
  final bool otp;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;

  FormFeildUserDetails(
      {this.labelText,
      this.textEditingController,
      this.onValueChanged,
      this.otp,
      this.isPassword});
  @override
  _FormFeildUserDetailsState createState() => _FormFeildUserDetailsState();
}

class _FormFeildUserDetailsState extends State<FormFeildUserDetails> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: screenHeight(context, dividedBy: 16),
        child: TextField(
          controller: widget.textEditingController,
          cursorColor: Constants.kitGradients[29],
          obscureText: widget.isPassword ? true : false,
          keyboardType: widget.otp ? TextInputType.number : TextInputType.text,
          style: TextStyle(
            color: Constants.kitGradients[30],
            fontFamily: 'OpenSansRegular',
            fontSize: 16,
          ),
          decoration: InputDecoration(
            labelText: widget.labelText,
            labelStyle: TextStyle(
              color: Constants.kitGradients[29],
              fontFamily: 'OpenSansRegular',
              fontSize: 16,
            ),
            fillColor: Colors.white,

            focusedBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Constants.kitGradients[29], width: 0.0),
              borderRadius: BorderRadius.circular(20.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: Constants.kitGradients[29], width: 0.0),
              borderRadius: BorderRadius.circular(20.0),
            ),
            // enabledBorder: InputBorder(
            //    BorderSide(color: Constants.kitGradients[29], width: 1.0),
            //   borderRadius: BorderRadius.circular(10.0),
            // )
            // UnderlineInputBorder(
            //   borderSide: BorderSide(color: Constants.kitGradients[29]),
            // ),
          ),
        ));
  }
}
