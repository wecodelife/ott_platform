import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CustomBottomBar extends StatefulWidget {
  final Function onTapSearch;
  final Function onTapProfile;
  final Function onTapHome;
  final double currentIndex;
  final Function onTapUpcoming;
  final Function onMarketPlace;
  CustomBottomBar(
      {this.onTapHome,
      this.onTapProfile,
      this.onTapSearch,
      this.onTapUpcoming,
      this.currentIndex,
      this.onMarketPlace});
  @override
  _CustomBottomBarState createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {
  int count;
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 12),
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 15),
        ),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Constants.kitGradients[24],
          border: Border(
            top: BorderSide(color: Constants.kitGradients[32]),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                widget.onTapHome();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.home_outlined,
                      color: widget.currentIndex == 0
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                      size: 25),
                  Text(
                    "Home",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: widget.currentIndex == 0
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                widget.onTapSearch();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.search_outlined,
                      color: widget.currentIndex == 1
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                      size: 25),
                  Text(
                    "Search",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: widget.currentIndex == 1
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                widget.onTapUpcoming();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.video_collection_outlined,
                      color: widget.currentIndex == 2
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                      size: 25),
                  Text(
                    "Upcoming",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: widget.currentIndex == 2
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                widget.onMarketPlace();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.add_business_outlined,
                      color: widget.currentIndex == 3
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                      size: 25),
                  Text(
                    "Market",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: widget.currentIndex == 3
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                widget.onTapProfile();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.person_outlined,
                      color: widget.currentIndex == 4
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                      size: 25),
                  Text(
                    "Profile",
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: widget.currentIndex == 4
                          ? Constants.kitGradients[29]
                          : Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
