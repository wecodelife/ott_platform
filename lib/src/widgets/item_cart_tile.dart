import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/image_slider.dart';
import 'package:flutter/services.dart';
import 'package:app_template/src/screens/data_page.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ItemCartTile extends StatefulWidget {
  final String itemName;
  final String itemImage;
  final String itemPrice;
  ItemCartTile({this.itemPrice, this.itemName, this.itemImage});
  @override
  _ItemCartTileState createState() => _ItemCartTileState();
}

class _ItemCartTileState extends State<ItemCartTile> {
  int count = 1;
  int itemCount = 0;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {},
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 40),
              ),
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                vertical: screenHeight(context, dividedBy: 50),
                horizontal: screenWidth(context, dividedBy: 60),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Constants.kitGradients[28],
              ),
              child: Row(
                children: [
                  CachedNetworkImage(
                    fit: BoxFit.fill,
                    placeholder: (context, url) => Center(
                      heightFactor: 1,
                      widthFactor: 1,
                      child: SizedBox(
                        height: 16,
                        width: 16,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(
                              Constants.kitGradients[29]),
                          strokeWidth: 2,
                        ),
                      ),
                    ),
                    imageUrl: widget.itemImage,
                    imageBuilder: (context, imageProvider) => Container(
                      child: Column(
                        children: [
                          Container(
                            width: screenWidth(context, dividedBy: 4.5),
                            height: screenWidth(context, dividedBy: 4.5),
                            decoration: BoxDecoration(
                              color: Constants.kitGradients[29],
                              borderRadius: BorderRadius.circular(7),
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                          // SizedBox(
                          //     height: screenHeight(context,
                          //         dividedBy: 80)),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 30),
                  ),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.itemName,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color:  Constants.kitGradients[27],
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 200),
                        ),
                        Text(
                          "₹ " + widget.itemPrice,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color:  Constants.kitGradients[27]
                               ,
                          ),
                        )
                      ]),
                  Spacer(),
                  Column(
                    children: [
                      Text(
                        "No of items: ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color:  Constants.kitGradients[27]
                             ,
                        ),
                      ),
                      SizedBox(height: screenHeight(context, dividedBy: 80)),
                      Container(
                          width: screenWidth(context, dividedBy: 4),
                          height: screenWidth(context, dividedBy: 10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color:Constants.kitGradients[29],
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                  child: Icon(
                                    Icons.remove,
                                    color:  Constants.kitGradients[27],
                                  ),
                                  onTap: () {
                                    setState(() {
                                      itemCount--;
                                    });
                                  }),
                              Text(
                                itemCount.toString(),
                                style: TextStyle(
                                  //decoration: TextDecoration.underline,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400,
                                  color:  Constants.kitGradients[27],
                                ),
                              ),
                              GestureDetector(
                                  child: Icon(
                                    Icons.add,
                                    color:  Constants.kitGradients[27],
                                  ),
                                  onTap: () {
                                    setState(() {
                                      itemCount++;
                                    });
                                  }),
                            ],
                          ))
                    ],
                  )
                ],
              ),
            ),
            SizedBox(height: screenHeight(context, dividedBy: 80)),
          ],
        ));
  }
}
