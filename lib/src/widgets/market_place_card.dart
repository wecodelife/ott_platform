import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MarketPlaceCard extends StatefulWidget {
  final String imgUrl;
  final Function itemPressed;
  final String catType;
  final bool circle;
  MarketPlaceCard({this.imgUrl, this.itemPressed, this.catType, this.circle});
  @override
  _MarketPlaceCardState createState() => _MarketPlaceCardState();
}

class _MarketPlaceCardState extends State<MarketPlaceCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: widget.itemPressed,
        child: CachedNetworkImage(
          width: screenWidth(context, dividedBy: 4.5),
          height: screenWidth(context, dividedBy: 3),
          fit: BoxFit.fill,
          placeholder: (context, url) => Center(
            heightFactor: 1,
            widthFactor: 1,
            child: SizedBox(
              height: 16,
              width: 16,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Constants.kitGradients[29]),
                strokeWidth: 2,
              ),
            ),
          ),
          imageUrl: widget.imgUrl,
          imageBuilder: (context, imageProvider) => Container(
            child: Column(
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 4.5),
                  height: screenWidth(context, dividedBy: 4.5),
                  decoration: BoxDecoration(
                    //shape: widget.circle ? BoxShape.circle : BoxShape.rectangle,
                    color: Constants.kitGradients[28],
                    borderRadius: widget.circle
                        ? BorderRadius.circular(
                            screenWidth(context, dividedBy: 4.5) / 2)
                        : BorderRadius.circular(7),
                    //shape: BoxShape.circle,
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover),
                  ),
                ),
                SizedBox(height: screenHeight(context, dividedBy: 80)),
                Text(
                  widget.catType,
                  style: TextStyle(
                    color: Constants.kitGradients[27],
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
