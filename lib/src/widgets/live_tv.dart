import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class LiveTv extends StatefulWidget {
  final String img;
  LiveTv({this.img});
  @override
  _LiveTvState createState() => _LiveTvState();
}

class _LiveTvState extends State<LiveTv> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 80),
        ),
        //alignment: Alignment.center,
        // padding: EdgeInsets.symmetric(
        //   horizontal: screenWidth(context, dividedBy: 60),
        // ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Stack(
          children: [
            Container(
              height: screenWidth(context, dividedBy: 3),
              width: screenWidth(context, dividedBy: 1.291),
              child: ClipRRect(
                // borderRadius: BorderRadius.circular(6),
                child: CachedNetworkImage(
                  placeholder: (context, url) => Center(
                    // heightFactor: 1,
                    // widthFactor: 1,
                    child: SizedBox(
                      height: 16,
                      width: 16,
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation(Constants.kitGradients[29]),
                        strokeWidth: 2,
                      ),
                    ),
                  ),
                  imageUrl: widget.img,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Constants.kitGradients[28],
                      //shape: BoxShape.circle,
                      image: DecorationImage(
                          image: imageProvider, fit: BoxFit.fill),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: Container(
                height: screenWidth(context, dividedBy: 12),
                width: screenWidth(context, dividedBy: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topRight: Radius.circular(6)),
                  color: Constants.kitGradients[3],
                ),
                child: Column(
                  children: [
                    Icon(
                      Icons.live_tv_outlined,
                      size: 20,
                      color: Constants.kitGradients[27],
                    ),
                    Text(
                      "Live",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 6,
                        fontWeight: FontWeight.bold,
                        color: Constants.kitGradients[27],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
