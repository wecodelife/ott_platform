import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';

class WebSeriesTile extends StatefulWidget {
  final int seriesCount;
  final String subTitle;
  final String seriesImages;
  final String seriesDescription;
  WebSeriesTile(
      {this.seriesCount,
      this.subTitle,
      this.seriesImages,
      this.seriesDescription});
  @override
  _WebSeriesTileState createState() => _WebSeriesTileState();
}

class _WebSeriesTileState extends State<WebSeriesTile> {
  int count = 1;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            count == 1
                ? setState(() {
                    count = 2;
                  })
                : setState(() {
                    count = 1;
                  });
          },
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 12),
            decoration: BoxDecoration(
                color: Constants.kitGradients[28],
                borderRadius: BorderRadius.circular(4)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: screenWidth(context, dividedBy: 30)),
                Icon(Icons.play_circle_fill_rounded,
                    color: Constants.kitGradients[30]),
                SizedBox(width: screenWidth(context, dividedBy: 30)),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: screenHeight(context, dividedBy: 100)),
                    Text(
                      "Episode ${widget.seriesCount + 1}",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                        color: Constants.kitGradients[30],
                      ),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 90)),
                    // Text(
                    //   widget.subTitle,
                    //   style: TextStyle(
                    //     fontSize: 13,
                    //     fontWeight: FontWeight.w400,
                    //     color: Constants.kitGradients[30],
                    //   ),
                    // ),
                  ],
                ),
              ],
            ),
          ),
        ),
        count == 2
            ? Container(
                width: screenWidth(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                    vertical: screenHeight(context, dividedBy: 60),
                    horizontal: screenWidth(context, dividedBy: 40)),
                decoration: BoxDecoration(
                    color: Constants.kitGradients[28],
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(4),
                        bottomLeft: Radius.circular(4))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(7),
                      child: CachedNetworkImage(
                        fit: BoxFit.fill,
                        imageUrl: widget.seriesImages,
                        imageBuilder: (context, imageProvider) => Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 3.5),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        placeholder: (context, url) => Center(
                          heightFactor: 1,
                          widthFactor: 1,
                          child: SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  Constants.kitGradients[32]),
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    Text(
                      "DESCRIPTION :",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                        color: Constants.kitGradients[30],
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 70),
                    ),
                    Text(
                      widget.seriesDescription,
                      //textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Constants.kitGradients[30],
                      ),
                    ),
                  ],
                ))
            : Container()
      ],
    );
  }
}
