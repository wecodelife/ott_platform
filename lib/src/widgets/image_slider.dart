import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:cached_network_image/cached_network_image.dart';

class CustomImageSlider extends StatefulWidget {
  final List<String> sliderImages;
  CustomImageSlider({this.sliderImages});
  @override
  _CustomImageSliderState createState() => _CustomImageSliderState();
}

class _CustomImageSliderState extends State<CustomImageSlider> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Carousel(
      boxFit: BoxFit.fill,
      images: widget.sliderImages
          .map(
            (item) =>
                CachedNetworkImage(
              fit: BoxFit.fill,
              imageUrl: item,
              imageBuilder: (context, imageProvider) => Container(
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fill,
                      ),
                    ),
                  
              ),
              placeholder: (context, url) => Center(
                heightFactor: 1,
                widthFactor: 1,
                child: SizedBox(
                  height: 16,
                  width: 16,
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation(Constants.kitGradients[32]),
                    strokeWidth: 2,
                  ),
                ),
              ),
            ),
          )
          .toList(),
      dotSize: 5.0,
      dotIncreaseSize: 2,
      dotSpacing: 20.0,
      dotColor: Colors.grey[850],
      dotBgColor: Colors.transparent,
      dotIncreasedColor: Color(0xff525B56),
      //moveIndicatorFromBottom: 8.0,
      dotVerticalPadding: screenHeight(context, dividedBy: 120),
      indicatorBgPadding: 2.0,
      autoplayDuration: Duration(seconds: 4),
    ));
  }
}
