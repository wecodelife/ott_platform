import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';

class SearchBarTile extends StatefulWidget {
  @override
  _SearchBarTileState createState() => _SearchBarTileState();
}

class _SearchBarTileState extends State<SearchBarTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 15),
      //color:Colors.blue,
      decoration:BoxDecoration(
        color: Constants.kitGradients[28],
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          Icon(
            Icons.search_outlined,
            color: Constants.kitGradients[30],
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 60),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1.4),
            child:TextField(
            cursorColor: Constants.kitGradients[32],
            style: TextStyle(
              color: Constants.kitGradients[30],
              fontWeight: FontWeight.w400,
              fontSize: 16,
            ),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(
                 // horizontal: screenWidth(context, dividedBy: 50),
                  vertical: screenHeight(context, dividedBy: 50)),
              focusColor: Constants.kitGradients[29],
              hintText: "Search",
              hintStyle: TextStyle(
                color: Constants.kitGradients[30],
                fontWeight: FontWeight.w400,
                fontSize: 16,
              ),
              border: InputBorder.none,
            ),
          ),),
        ],
      ),
    );
  }
}
