class Urls {
  static final baseUrl = "http://3.142.247.21:7000/api/v1/";
  static final registerUrl = baseUrl+"users/";
  static final loginUrl = baseUrl+"login/";
}
