import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/market_place_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/video_player.dart';
import 'package:flutter/material.dart';

List<String> videos = [
  "https://www.ibrahimabah.com/ibfilms/Harry.Potter-The.Ultimate.Collection.%20I%20-%20VIII%20.%202001-2011.1080p.Bluray.x264.anoXmous/Harry%20Potter%208/Harry.Potter.And.The.Deathly.Hallows.Part.2.2011.1080p.Bluray.x264.anoXmous_.mp4",
  "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
  "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4",
];
final List<String> titles = [
  "Harry Potter",
  "Mortal Kombat",
  "RockStar",
];
final List<String> movieType = [
  "Action",
  "Action",
  "Drama",
];
final List<String> imgs = [
  "https://cdn.dribbble.com/users/3408570/screenshots/10746910/media/25b3567e37c225bad3dfb92133e1dc36.jpg?compress=1&resize=800x600",
  "https://www.joblo.com/assets/images/joblo/news/2021/03/mortal-kombat-2021-poster-group.jpg",
  "https://hdfreewallpaper.net/wp-content/uploads/2015/03/movies-wallpaper.jpg",
];
final List<String> rating = [
  "8.2",
  "7.4",
  "6.4",
];
final List<String> duration = [
  "2 hr 20 min",
  "1 hr 19 min",
  "2 hr 3 min",
];
final List<String> episodeSubtitles = [
  "Episode 1 Description",
  "Episode 2 Description",
  "Episode 3 Description",
  "Episode 4 Description",
  "Episode 5 Description",
  "Episode 6 Description",
];

class UpcomingVideos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children:[
          Column(children: [
            Container(
                padding: EdgeInsets.only(bottom: screenHeight(context,dividedBy: 10)),
                color: Constants.kitGradients[24],
                height: screenHeight(context, dividedBy: 1),
                width: screenWidth(context,dividedBy: 1),
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: videos.length,
                  itemBuilder: (context, index) {
                    return Column(
                        children: [
                          Container(
                              height: screenHeight(context, dividedBy: 1.8),
                              // width: screenWidth(context, dividedBy: 1),
                              margin: EdgeInsets.symmetric(
                                horizontal: screenWidth(context, dividedBy: 50),
                              ),
                              alignment: Alignment.center,
                              padding: EdgeInsets.symmetric(
                                vertical: screenHeight(context, dividedBy: 60),
                                horizontal: screenWidth(context, dividedBy: 50),
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Constants.kitGradients[28],
                              ),
                              child: VideoWidget(
                                url: videos[index],
                                title: titles[index],
                                movieType: movieType[index],
                                description:
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                                ind: index,
                              )),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 35),
                          ),
                        ],
                    );
                  },
                )),
          ]),
          Positioned(
            bottom: 0,
            child: CustomBottomBar(
              currentIndex: 2,
              onTapSearch: () {
                pushAndReplacement(context, SearchPage());
              },
              onTapProfile: () {
                pushAndReplacement(
                    context,
                    ProfilePage(
                        username: "Brandon Jake",
                        email: "Brandon@example.com",
                        profileImage:
                            "https://images.unsplash.com/photo-1597466765990-64ad1c35dafc"));
              },
              onTapHome: () {
                pushAndReplacement(context, HomePage());
              },
              onMarketPlace: () {
                pushAndReplacement(context, MarketPlacePage());
              },
              onTapUpcoming: () {},
            ),
          ),
        ],
      ),
    );
  }
}
