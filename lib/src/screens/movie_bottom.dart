import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/screens/trending_list_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/movie_details_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/search_page.dart';

class MovieBottom extends StatefulWidget {
  final String movieTitle;
  final String subTitle;
  final String imgUrl;
  final String description;
  final String rating;
  final String time;
  final bool isSeries;
  final String seriesSub;
  MovieBottom({this.movieTitle, this.subTitle, this.imgUrl, this.description, this.rating, this.time, this.isSeries, this.seriesSub});
  @override
  _MovieBottomState createState() => _MovieBottomState();
}

class _MovieBottomState extends State<MovieBottom> {

   int _pageIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      if (index == 0) {
        _pageIndex = 10;
        print(_pageIndex);
      } else {
        _pageIndex = index;
        print(_pageIndex);
      }
    });
  }
   Widget pageCaller(int index) {
    switch (index) {
      case 0:
        {
          return MovieDetails(
            movieTitle: widget.movieTitle,
            subTitle: widget.subTitle,
            imgUrl: widget.imgUrl,
            description: widget.description,
            rating: widget.rating,
            time:widget.time,
            isSeries: widget.isSeries,
            seriesSub: widget.seriesSub
          );
        }

      case 1:
        {
          return SearchPage();
        }
      case 2:
        {
          return ProfilePage(username: "Brandon Jake", email: "Brandon@example.com",profileImage:  "https://images.unsplash.com/photo-1597466765990-64ad1c35dafc");
        }
      case 10:
        {
          return HomePage();
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageCaller(_pageIndex),
      bottomNavigationBar: StyleProvider(
        style: Style(context: context),
        child: ConvexAppBar(
          style: TabStyle.react,
          color: Colors.grey,
          initialActiveIndex: 0,
          height: screenHeight(context, dividedBy: 13),
          backgroundColor: Constants.kitGradients[24],
          activeColor: Constants.kitGradients[29],
          curve: Curves.bounceIn,
          items: [
            TabItem(
              icon: Container(
                width:screenWidth(context, dividedBy:1),
                child:Icon(
                Icons.home_outlined,
                color: Colors.grey,
                size: 25,
              ),),
              activeIcon: Icon(
                Icons.home_outlined,
                color: Constants.kitGradients[29],
                size: 30,
              ),
              title: "Home",
            ),
            TabItem(
              icon: Container(
                width:screenWidth(context, dividedBy:1),
                child:Icon(Icons.search_outlined, size: 25, color: Colors.grey),),
              activeIcon: Icon(
                Icons.search_outlined,
                color: Constants.kitGradients[29],
                size: 30,
              ),
              title: "Search",
            ),
            TabItem(
                icon: Container(
                width:screenWidth(context, dividedBy:1),
                child:Center(child:Icon(
                  Icons.person_outline,
                  color: Colors.grey,
                  size: 25,
                ),)),
                activeIcon: Icon(Icons.person_outline,
                    color: Constants.kitGradients[29], size: 30),
                title: "Profile"),
          ],
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}

class Style extends StyleHook {
  BuildContext context;
  Style({this.context});
  @override
  double get activeIconSize => 40;

  @override
  double get activeIconMargin => 40;
  
   @override
  double get iconSize => 40;

  @override
  TextStyle textStyle(Color color) {
    return TextStyle(
        fontSize: screenWidth(context, dividedBy: 30), color: color);
  }
}
