import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/market_place_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/upcoming_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/searchbar_tile.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<String> searchResults = [
    "Godzilla",
    "Evaru",
    "Dhrishyam 2",
    "Joji",
    "Badla",
    "Panga",
    "Master",
    "Venom"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[24],
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    vertical: screenHeight(context, dividedBy: 30)),
                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    SearchBarTile(),
                    Container(
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: searchResults.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            children: [
                              Container(
                                width: screenWidth(context, dividedBy: 1),
                                //height: screenHeight(context, dividedBy: 13),
                                padding: EdgeInsets.symmetric(
                                  vertical:
                                      screenHeight(context, dividedBy: 60),
                                  horizontal:
                                      screenWidth(context, dividedBy: 30),
                                ),
                                alignment: Alignment.centerLeft,
                                decoration: BoxDecoration(),
                                child: Text(
                                  searchResults[index],
                                  style: TextStyle(
                                    color: Constants.kitGradients[30],
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                              Divider(
                                color: Constants.kitGradients[32],
                                thickness: 1,
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: CustomBottomBar(
                currentIndex: 1,
                onTapSearch: () {},
                onTapProfile: () {
                  push(
                      context,
                      ProfilePage(
                          username: "Brandon Jake",
                          email: "Brandon@example.com",
                          profileImage:
                              "https://images.unsplash.com/photo-1597466765990-64ad1c35dafc"));
                },
                onTapHome: () {
                  push(context, HomePage());
                },
                onMarketPlace: () {
                  push(context, MarketPlacePage());
                },
                onTapUpcoming: () {
                  push(context, UpcomingVideos());
                },
              ),
            )
          ],
        ));
  }
}
