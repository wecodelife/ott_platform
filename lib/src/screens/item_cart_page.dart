import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/widgets/item_cart_tile.dart';

class ItemCartPage extends StatefulWidget {
  final String itemName;
  final String itemImage;
  final String itemPrice;
  ItemCartPage({this.itemPrice, this.itemName, this.itemImage});
  @override
  _ItemCartPageState createState() => _ItemCartPageState();
}

class _ItemCartPageState extends State<ItemCartPage> {
  int count = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[24],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[24],
          leading: GestureDetector(
            onTap: () {
              pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Constants.kitGradients[27],
            ),
          ),
          title: Container(
            child: Row(
              children: [
                Text(
                  "Your Cart",
                  style: TextStyle(
                    //fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Constants.kitGradients[27],
                  ),
                ),
                Spacer(),
                GestureDetector(
                    child: Icon(
                  Icons.notifications_outlined,
                  color: Constants.kitGradients[27],
                ))
              ],
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: 4,
                  itemBuilder: (BuildContext context, int index) {
                    return ItemCartTile(
                        itemName: widget.itemName,
                        itemImage: widget.itemImage,
                        itemPrice: widget.itemPrice);
                  },
                ),
              ),
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  onTap: () {
                    count == 1
                        ? setState(() {
                            count = 2;
                          })
                        : setState(() {
                            count = 1;
                          });
                  },
                  child:
                       Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 15),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[29],
                      border: Border.all(
                        color: Constants.kitGradients[29],
                      ),
                    ),
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 30),
                      vertical: screenHeight(context,dividedBy:50),
                    ),
                    child: Text(
                      "PLACE ORDER",
                      style: TextStyle(
                        color: Constants.kitGradients[27],
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  
                    
                ))
          ],
        ));
  }
}
