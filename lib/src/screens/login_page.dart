import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/sign_up_personal_info_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/form_field_user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/login_request_model.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();

  UserBloc userBloc = UserBloc();
  bool loading = false;

  @override
  void initState() {
    // TODO: implement initState
    userBloc.loginResponse.listen((event) {
      print("Done");
      push(context, HomePage());
      setState(() {
        loading = false;
      });
    });
    super.initState();
  }

//firebase integration
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  String name = 'Log In';
  Future<User> _signIn() async {
    // GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    // GoogleSignInAuthentication gSA  = await googleSignInAccount.authentication;
    final GoogleSignInAccount account = await googleSignIn.signIn();
    final GoogleSignInAuthentication authentication =
        await account.authentication;

    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        idToken: authentication.idToken,
        accessToken: authentication.accessToken);

    final UserCredential authResult =
        await _auth.signInWithCredential(credential);
    final User user = authResult.user;

    print("UserName :${user.displayName}");
    setState(() {
      name = user.displayName.toString();
    });
    return user;
  }

  void handleLogin() async {
    User user = await _signIn();
    // Here signInWithGoogle() is your defined function!
    if (user != null) {
      pushAndRemoveUntil(context, HomePage(),false);
      print("Logged User:${user.displayName}");
    } else {
      print("Error in your Google Account");
    }
  }

  void _signOut() {
    googleSignIn.signOut();
    print("Signed out Successfully");
  }

  @override
  Widget build(BuildContext context) {
    return loading == true
        ? Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            color: Constants.kitGradients[24],
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : SafeArea(
            child: Scaffold(
              backgroundColor: Constants.kitGradients[24],
              body: SingleChildScrollView(
                child: Container(
                  width: screenWidth(context, dividedBy: 1),
                  alignment: Alignment.center,
                  color: Constants.kitGradients[24],
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 3.5),
                      ),
                      FormFeildUserDetails(
                        labelText: "Email / Mobile Number",
                        textEditingController: emailTextEditingController,
                        otp: false,
                        isPassword: false,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      FormFeildUserDetails(
                        labelText: "Password",
                        textEditingController: passwordTextEditingController,
                        otp: false,
                        isPassword: true,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 2.5),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          color: Constants.kitGradients[29],
                          child: Center(
                            child: Text(
                              "Login",
                              style: TextStyle(
                                color: Constants.kitGradients[30],
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                              ),
                            ),
                          ),
                          onPressed: () {
                            setState(() {
                              loading = true;
                            });
                            userBloc.login(
                                loginRequest: LoginRequest(
                                    email: emailTextEditingController.text,
                                    password:
                                        passwordTextEditingController.text));
                           // push(context, HomePage());
                          },
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      GestureDetector(
                        onTap: () {
                          //push(context, SignUpPage());
                        },
                        child: Text(
                          "Forgot Password",
                          style: TextStyle(
                            color: Constants.kitGradients[29],
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      GestureDetector(
                        onTap: () {
                          push(context, SignUpPersonalInfoPage());
                        },
                        child: Text(
                          "Are you New Here ? Sign Up",
                          style: TextStyle(
                            color: Constants.kitGradients[29],
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Text(
                        "Or Sign In with Google Account",
                        style: TextStyle(
                          color: Constants.kitGradients[30],
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      GestureDetector(
                        onTap: () {
                          handleLogin();
                        },
                        child: SvgPicture.asset(
                          "assets/icons/google.svg",
                          height: screenHeight(context, dividedBy: 15),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
