import 'dart:async';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:app_template/main.dart';
import 'package:app_template/src/screens/live_streaming.dart';
import 'package:app_template/src/screens/market_place_page.dart';
import 'package:app_template/src/screens/movie_details_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/screens/trending_list_page.dart';
import 'package:app_template/src/screens/upcoming_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/heading_tile.dart';
import 'package:app_template/src/widgets/horizontal_list_tile.dart';
import 'package:app_template/src/widgets/image_slider.dart';
import 'package:app_template/src/widgets/live_tv.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:permission_handler/permission_handler.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {
  String channelId="test";
  final List<String> imgList = [
    "https://cdn.dribbble.com/users/3408570/screenshots/10746910/media/25b3567e37c225bad3dfb92133e1dc36.jpg?compress=1&resize=800x600",
    "https://www.joblo.com/assets/images/joblo/news/2021/03/mortal-kombat-2021-poster-group.jpg",
    "https://s3media.freemalaysiatoday.com/wp-content/uploads/2020/08/Tenet-lifestyle-260820-561x420.png",
    "https://hdfreewallpaper.net/wp-content/uploads/2015/03/latest-movies.jpg",
    "https://hdfreewallpaper.net/wp-content/uploads/2015/03/movies-wallpaper.jpg",
  ];
  final List<String> liveImgs = [
    "https://hdwallpaper.net/web/wallpapers/goku-dragon-ball-z-wallpaper-138/thumbnail/lg.jpg",
    "https://hdwallpaper.net/web/wallpapers/snowy-crow-anime-wallpaper-514/thumbnail/lg.jpg",
    "https://hdwallpaper.net/web/wallpapers/naruto-kakashi-hatake-wallpaper-401/thumbnail/lg.jpg",
  ];
  final List<String> imgs = [
    "https://cdn.dribbble.com/users/3408570/screenshots/10746910/media/25b3567e37c225bad3dfb92133e1dc36.jpg?compress=1&resize=800x600",
    "https://www.joblo.com/assets/images/joblo/news/2021/03/mortal-kombat-2021-poster-group.jpg",
    "https://hdfreewallpaper.net/wp-content/uploads/2015/03/movies-wallpaper.jpg",
  ];

  List<String> videos = [
    "https://www.ibrahimabah.com/ibfilms/Harry.Potter-The.Ultimate.Collection.%20I%20-%20VIII%20.%202001-2011.1080p.Bluray.x264.anoXmous/Harry%20Potter%208/Harry.Potter.And.The.Deathly.Hallows.Part.2.2011.1080p.Bluray.x264.anoXmous_.mp4",
    "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
    "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4",
  ];
  final List<String> titles = [
    "BATMAN",
    "Mortal Kombat",
    "RockStar",
  ];
  final List<String> movieType = [
    "Action",
    "Action",
    "Drama",
  ];

  final List<String> rating = [
    "8.2",
    "7.4",
    "6.4",
  ];
  final List<String> duration = [
    "2 hr 20 min",
    "1 hr 19 min",
    "2 hr 3 min",
  ];
  final List<String> category = [
    "CATEGORY 1",
    "CATEGORY 2",
  ];
  final List<String> episodeSubtitles = [
    "Episode 1 Description",
    "Episode 2 Description",
    "Episode 3 Description",
    "Episode 4 Description",
    "Episode 5 Description",
    "Episode 6 Description",
  ];
  ScrollController _scrollController = ScrollController();
  _scrollToBottom() {
    if (_scrollController.hasClients) {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 30000), curve: Curves.elasticOut);
    } else {
      Timer(Duration(milliseconds: 400), () => _scrollToBottom());
    }
  }
  // Future<void> onJoin({bool isBroadcaster}) async {
  //   await [Permission.camera, Permission.microphone].request();
  //
  //   push(context, LiveStreaming(channel: channelId,
  //     role: true,));
  //   print("CHANNEL ID"+channelId);
  // }
  Future<void> onJoin({bool isBroadcaster}) async {
    await [Permission.camera, Permission.microphone].request();

    push(context, BroadcastPage(channelName: channelId,isBroadcaster: isBroadcaster,));
    print("CHANNEL ID"+channelId);
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => _scrollToBottom());
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
        ),
        child: Scaffold(
            body: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          color: Constants.kitGradients[24],
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: screenHeight(context, dividedBy: 3),
                      width: screenWidth(context, dividedBy: 1),
                      // color: Constants.kitGradients[24],
                      child: CustomImageSlider(
                        sliderImages: imgList,
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height: screenWidth(context, dividedBy: 3),
                            width: screenWidth(context, dividedBy: 1),
                            child: ListView.builder(
                                shrinkWrap: true,
                                controller: _scrollController,
                                scrollDirection: Axis.horizontal,
                                itemCount: liveImgs.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    height: screenWidth(context, dividedBy: 3),
                                    width: screenWidth(context, dividedBy: 1.8),
                                    child: GestureDetector(
                                        child: LiveTv(img: liveImgs[index]),
                                        onTap: () {
                                          onJoin(isBroadcaster: false);
                                        }),
                                  );
                                }),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 1),
                            margin: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 40),
                            ),
                            alignment: Alignment.center,
                            padding: EdgeInsets.symmetric(
                              vertical: screenHeight(context, dividedBy: 50),
                              horizontal: screenWidth(context, dividedBy: 60),
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Constants.kitGradients[28],
                            ),
                            child: Column(
                              children: [
                                HeadingTile(
                                  heading: "TRENDING",
                                  onPressed: () {
                                    push(
                                        context,
                                        TrendingList(
                                          pageTitle: "TRENDING",
                                          selection: true,
                                        ));
                                  },
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 30),
                                ),
                                Container(
                                  height: screenWidth(context, dividedBy: 3.5),
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: imgs.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Row(
                                        children: [
                                          HorizontalListTile(
                                            imgUrl: imgs[index],
                                            cardPressed: () {
                                              push(
                                                context,
                                                MovieDetails(
                                                  movieTitle: titles[index],
                                                  subTitle: movieType[index],
                                                  imgUrl: imgs[index],
                                                  description:
                                                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                                                  rating: rating[index],
                                                  time: duration[index],
                                                  isSeries: true,
                                                  seriesSub:
                                                      episodeSubtitles[index],
                                                  videoUrl: videos[index],
                                                ),
                                              );
                                            },
                                          ),
                                          SizedBox(
                                            width: screenWidth(context,
                                                dividedBy: 40),
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 1),
                            alignment: Alignment.center,
                            color: Constants.kitGradients[24],
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: category.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  //width: screenWidth(context, dividedBy: 1),
                                  margin: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 40),
                                    vertical:
                                        screenHeight(context, dividedBy: 50),
                                  ),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.symmetric(
                                    vertical:
                                        screenHeight(context, dividedBy: 50),
                                    horizontal:
                                        screenWidth(context, dividedBy: 60),
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: Constants.kitGradients[28],
                                  ),
                                  child: Column(
                                    children: [
                                      HeadingTile(
                                        heading: category[index],
                                        onPressed: () {
                                          push(
                                              context,
                                              TrendingList(
                                                selection: true,
                                                pageTitle: category[index],
                                              ));
                                        },
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 60),
                                      ),
                                      Container(
                                        height: screenWidth(context,
                                            dividedBy: 3.5),
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: imgs.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Row(
                                              children: [
                                                HorizontalListTile(
                                                  imgUrl: imgs[index],
                                                  cardPressed: () {
                                                    push(
                                                      context,
                                                      MovieDetails(
                                                        movieTitle:
                                                            titles[index],
                                                        subTitle:
                                                            movieType[index],
                                                        imgUrl: imgs[index],
                                                        description:
                                                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                                                        rating: rating[index],
                                                        time: duration[index],
                                                        isSeries: false,
                                                        videoUrl: videos[index],
                                                      ),
                                                    );
                                                  },
                                                ),
                                                SizedBox(
                                                  width: screenWidth(context,
                                                      dividedBy: 40),
                                                ),
                                              ],
                                            );
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 10),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                child: CustomBottomBar(
                  currentIndex: 0,
                  onTapSearch: () {
                    push(context, SearchPage());
                  },
                  onTapUpcoming: () {
                    push(context, UpcomingVideos());
                  },
                  onMarketPlace: () {
                    push(context, MarketPlacePage());
                  },
                  onTapProfile: () {
                    push(
                        context,
                        ProfilePage(
                            username: "Brandon Jake",
                            email: "Brandon@example.com",
                            profileImage:
                                "https://images.unsplash.com/photo-1597466765990-64ad1c35dafc"));
                  },
                  onTapHome: () {},
                ),
              ),
              new Positioned(
                  bottom: 100,
                  right: 5,
                  child: FloatingActionButton(
                    onPressed: () {
                      onJoin(isBroadcaster: true);
                    },
                    backgroundColor: Constants.kitGradients[3],
                    child: Center(
                      child: Column(
                        children: [
                          Icon(
                            Icons.live_tv,
                            size: 35,
                            color: Constants.kitGradients[30],
                          ),
                          Text(
                            "Go Live",
                            style: TextStyle(
                                fontSize: 10,
                                color: Constants.kitGradients[30]),
                          )
                        ],
                      ),
                    ),
                  )),
            ],
          ),
        )));
  }
}
