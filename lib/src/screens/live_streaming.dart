import 'dart:ui';

import 'package:app_template/src/config/agora.config.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;

class BroadcastPage extends StatefulWidget {
  final String channelName;
  final bool isBroadcaster;
  const BroadcastPage({Key key, this.channelName, this.isBroadcaster})
      : super(key: key);

  @override
  _BroadcastPageState createState() => _BroadcastPageState();
}

class _BroadcastPageState extends State<BroadcastPage> {
  final _users = <int>[];
  RtcEngine _engine;
  bool muted = false;
  bool switchCam = true;
  int viewCount;
  bool sound=true;
  bool blur=true;


  @override
  void dispose() {
    // clear users
    _users.clear();
    // destroy sdk and leave channel
    _engine.destroy();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // initialize agora sdk
    initializeAgora();
  }

  Future<void> initializeAgora() async {
    await _initAgoraRtcEngine();
    _engine.setEventHandler(RtcEngineEventHandler(
      joinChannelSuccess: (channel, uid, elapsed) {
        setState(() {
          print('onJoinChannel: $channel, uid: $uid');
        });
      },
      leaveChannel: (stats) {
        setState(() {
          print('onLeaveChannel');
          _users.clear();
        });
      },
      userJoined: (uid, elapsed) {
        setState(() {
          print('userJoined: $uid');

          _users.add(uid);
        });
      },
      userOffline: (uid, elapsed) {
        setState(() {
          print('userOffline: $uid');
          _users.remove(uid);
        });
      },
    ));

    await _engine.joinChannel(null, widget.channelName, null, 0);
  }

  Future<void> _initAgoraRtcEngine() async {
    _engine = await RtcEngine.createWithConfig(RtcEngineConfig(appId));
    await _engine.enableVideo();

    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    if (widget.isBroadcaster) {
      await _engine.setClientRole(ClientRole.Broadcaster);
    } else {
      await _engine.setClientRole(ClientRole.Audience);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: screenWidth(context,dividedBy: 1),
        height: screenHeight(context,dividedBy: 1),
        child: Center(
          child: Stack(
            children: <Widget>[
              _broadcastView(),
              blur? Container():Container(
                width: screenWidth(context,dividedBy: 1),
                height: screenHeight(context,dividedBy: 1),
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 50, sigmaY: 50),
                  child: Container(
                    color: Colors.black.withOpacity(0.1),
                  ),
                ),
              ),
              Positioned(
                  right: screenHeight(context, dividedBy: 100),
                  top: screenHeight(context, dividedBy: 2),
                  child: widget.isBroadcaster? Column(
                    children: [
                      InkWell(
                        onTap: _onToggleMute,
                        child: Icon(
                          muted ? Icons.mic_off : Icons.mic,
                          color: Constants.kitGradients[0],
                          size: screenHeight(context, dividedBy: 25),
                        ),
                      ),
                      SizedBox(height: screenHeight(context,dividedBy: 30),),
                      InkWell(
                        onTap: _onSwitchCamera,
                        child: Icon(Icons.flip_camera_ios_rounded,
                          color: Constants.kitGradients[0],
                          size: screenHeight(context,dividedBy: 25),
                        ),
                      ),
                      SizedBox(height: screenHeight(context,dividedBy: 30),),
                      InkWell(
                        onTap: (){
                          setState(() {
                            blur=!blur;
                          });
                        },
                        child: Icon(
                          blur ? Icons.videocam : Icons.videocam_off,
                          color: Constants.kitGradients[0],
                          size: screenHeight(context, dividedBy: 25),
                        ),
                      ),

                    ],
                  ):Column(
                    children: [
                      InkWell(
                        child: Icon(
                          Icons.favorite_border,
                          color: Constants.kitGradients[0],
                          size: screenHeight(context, dividedBy: 25),
                        ),
                      ),
                      SizedBox(height: screenHeight(context,dividedBy: 30),),
                      InkWell(
                        onTap: _volume,
                        child: Icon(
                          sound?Icons.volume_up:Icons.volume_off_sharp,
                          color: Constants.kitGradients[0],
                          size: screenHeight(context, dividedBy: 25),
                        ),
                      ),
                    ],
                  )),
              Positioned(
                top: screenHeight(context, dividedBy: 20),
                right: screenHeight(context, dividedBy: 40),
                child: Row(
                  children: [
                    Container(
                      height: screenHeight(context, dividedBy: 20),
                      // width: screenWidth(context,dividedBy: 4),
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[4].withOpacity(0.5),
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          Icon(
                            Icons.remove_red_eye_sharp,
                            color: Constants.kitGradients[7].withOpacity(0.5),
                            size: screenWidth(context, dividedBy: 15),
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 50),
                          ),
                          Text(
                            viewCount.toString(),
                            style: TextStyle(
                                color: Constants.kitGradients[0].withOpacity(0.8),
                                fontSize: screenWidth(context, dividedBy: 25)),
                          )
                        ],
                      ),
                    ),
                    SizedBox(width: screenHeight(context,dividedBy: 100),),
                    InkWell(
                      onTap: () {
                        _liveEnd(context);
                      },
                      child: Icon(
                        Icons.close_rounded,
                        color: Constants.kitGradients[0],
                        size: screenHeight(context, dividedBy: 18),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    height: screenHeight(context,dividedBy: 13),
                    width: screenWidth(context,dividedBy: 1),
                    decoration: BoxDecoration(
                      color: Constants.kitGradients[24].withOpacity(.5),
                      borderRadius: BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20))
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: screenWidth(context,dividedBy: 1.2),
                          height: screenHeight(context,dividedBy: 18),
                          child: TextField(
                            cursorColor: Constants.kitGradients[0],
                            cursorHeight: 20,
                            decoration: InputDecoration(
                                contentPadding:
                                EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                                hintText: "Comment...",
                              hintStyle: TextStyle(
                                fontSize: 16,
                                color: Constants.kitGradients[0]
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(width: 1,color: Constants.kitGradients[0]),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(width: 1,color: Constants.kitGradients[0]),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(width: 1,color: Constants.kitGradients[0]),
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                  borderSide: BorderSide(width: 1,color: Constants.kitGradients[0])
                              ),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                  borderSide: BorderSide(width: 1,color:Constants.kitGradients[0])
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                  borderSide: BorderSide(width: 1,color: Constants.kitGradients[0])
                              ),
                            ),
                            style: TextStyle(
                              fontSize: 16,
                              color: Constants.kitGradients[0]
                              ),
                            ),
                          ),
                        Spacer(),
                        Icon(Icons.send_outlined,
                          color: Constants.kitGradients[0],
                          size: screenHeight(context,dividedBy: 30),)
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<StatefulWidget> list = [];
    if (widget.isBroadcaster) {
      list.add(RtcLocalView.SurfaceView());
    }
    _users.forEach((int uid) => list.add(RtcRemoteView.SurfaceView(uid: uid)));
    setState(() {
      viewCount=list.length;
    });
    return list;
  }

  /// Video view row wrapper
  Widget _expandedVideoView(List<Widget> views) {
    final wrappedViews = views
        .map<Widget>((view) => Expanded(child: Container(child: view)))
        .toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Video layout wrapper
  Widget _broadcastView() {
    final views = _getRenderViews();
    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoView([views[0]])
          ],
        ));
      case 2:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoView([views[0]]),
            _expandedVideoView([views[1]])
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoView(views.sublist(0, 2)),
            _expandedVideoView(views.sublist(2, 3))
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoView(views.sublist(0, 2)),
            _expandedVideoView(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container();
  }

  void _liveEnd(BuildContext context) {
    Navigator.pop(context);
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    _engine.muteLocalAudioStream(muted);
  }

  void _onSwitchCamera() {
    switchCam=!switchCam;
    _engine.switchCamera();
  }
  void _volume() {
    setState(() {
      sound = !sound;
    });
    sound ? _engine.adjustPlaybackSignalVolume(0) : _engine
        .adjustPlaybackSignalVolume(200);
  }
}
