import 'package:app_template/src/screens/sign_up_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/form_field_user.dart';
import 'package:flutter/material.dart';

class SignUpPersonalInfoPage extends StatefulWidget {
  @override
  _SignUpPersonalInfoPageState createState() => _SignUpPersonalInfoPageState();
}

class _SignUpPersonalInfoPageState extends State<SignUpPersonalInfoPage> {
  TextEditingController firstNameTextEditingController =
      new TextEditingController();
  TextEditingController lastNameTextEditingController =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Constants.kitGradients[24],
            body: SingleChildScrollView(
              child: Container(
                  alignment: Alignment.center,
                  color: Constants.kitGradients[24],
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 3.5),
                        ),
                        FormFeildUserDetails(
                          labelText: "First Name",
                          textEditingController: firstNameTextEditingController,
                          otp: false,
                          isPassword: false,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        FormFeildUserDetails(
                          labelText: "Last Name",
                          textEditingController: lastNameTextEditingController,
                          otp: false,
                          isPassword: false,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 2.5),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            color: Constants.kitGradients[29],
                            child: Center(
                              child: Text(
                                "Next",
                                style: TextStyle(
                                  color: Constants.kitGradients[30],
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                            onPressed: () {
                              push(
                                  context,
                                  SignUpPage(
                                    firstName:
                                        firstNameTextEditingController.text,
                                    lastName:
                                        lastNameTextEditingController.text,
                                  ));
                            },
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 50),
                        ),
                        // GestureDetector(
                        //   onTap: () {
                        //     push(context, LoginPage());
                        //   },
                        //   child: Text(
                        //     "Already Have an Account ? Sign In",
                        //     style: TextStyle(
                        //       color: Constants.kitGradients[29],
                        //       fontWeight: FontWeight.w500,
                        //       fontSize: 16,
                        //     ),
                        //   ),
                        // )
                      ])),
            )));
  }
}
