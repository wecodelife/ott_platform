import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/register_user_request.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/form_field_user.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  final String firstName;
  final String lastName;
  SignUpPage({this.firstName, this.lastName});
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  TextEditingController confirmPasswordTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  UserBloc userBloc = UserBloc();
  bool loading = false;
  @override
  void initState() {
    userBloc.registerUserResponse.listen((event) {
      print("done");
      setState(() {
        loading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading == true
        ? Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            color: Constants.kitGradients[24],
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : SafeArea(
            child: Scaffold(
                backgroundColor: Constants.kitGradients[24],
                body: SingleChildScrollView(
                  child: Container(
                      alignment: Alignment.center,
                      color: Constants.kitGradients[24],
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 20)),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 3.5),
                            ),
                            FormFeildUserDetails(
                              labelText: "Email",
                              textEditingController: emailTextEditingController,
                              otp: false,
                              isPassword: false,
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 20),
                            ),
                            FormFeildUserDetails(
                              labelText: "Password",
                              textEditingController:
                                  passwordTextEditingController,
                              otp: false,
                              isPassword: true,
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 20),
                            ),
                            FormFeildUserDetails(
                              labelText: "Confirm Password",
                              textEditingController:
                                  confirmPasswordTextEditingController,
                              otp: false,
                              isPassword: true,
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 20),
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 2.5),
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                color: Constants.kitGradients[29],
                                child: Center(
                                  child: Text(
                                    "Send OTP",
                                    style: TextStyle(
                                      color: Constants.kitGradients[30],
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                                onPressed: () {
                                  setState(() {
                                    loading = true;
                                  });
                                  userBloc.registerUser(
                                      registerUserRequest: RegisterUserRequest(
                                          email:
                                              emailTextEditingController.text,
                                          lastName: widget.lastName,
                                          firstName: widget.firstName,
                                          password:
                                              passwordTextEditingController
                                                  .text,
                                          isVerified: true));
                                  // push(context, OTPInputPage());
                                },
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 50),
                            ),
                            GestureDetector(
                              onTap: () {
                                push(context, LoginPage());
                              },
                              child: Text(
                                "Already Have an Account ? Sign In",
                                style: TextStyle(
                                  color: Constants.kitGradients[29],
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16,
                                ),
                              ),
                            )
                          ])),
                )));
  }
}
