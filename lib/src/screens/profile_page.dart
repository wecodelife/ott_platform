import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/screens/market_place_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/screens/upcoming_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ProfilePage extends StatefulWidget {
  final String username;
  final String email;
  final String profileImage;
  ProfilePage({this.username, this.email, this.profileImage});
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[24],
      ),
      child: SafeArea(
        child: Scaffold(
            backgroundColor: Constants.kitGradients[24],
            body: Stack(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 1),
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      Container(
                        // height: screenHeight(context, dividedBy: 3),
                        width: screenWidth(context, dividedBy: 1),
                        color: Constants.kitGradients[24],
                        child: Column(
                          children: [
                            Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 15),
                              color: Colors.transparent,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: screenWidth(context, dividedBy: 30),
                                  ),
                                  Text(
                                    "PROFILE",
                                    style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w600,
                                      color: Constants.kitGradients[30],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: screenHeight(context,dividedBy: 8.2),),
                            Row(
                              children: [
                                SizedBox(width: screenWidth(context,dividedBy: 6),),
                                Container(
                                  height: screenWidth(context,dividedBy: 3.4),
                                  width: screenWidth(context, dividedBy: 1.3),
                                  decoration: BoxDecoration(
                                    color: Constants.kitGradients[28],
                                    borderRadius: BorderRadius.circular(10)
                                  ),
                                  padding: EdgeInsets.only(left: screenWidth(context,dividedBy: 5),
                                      top: screenWidth(context,dividedBy: 25),
                                      bottom: screenWidth(context,dividedBy: 20),),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        widget.username,
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w400,
                                          color: Constants.kitGradients[29],
                                        ),
                                      ),
                                      SizedBox(
                                        height: screenHeight(context, dividedBy: 70),
                                      ),
                                      Text(
                                        widget.email,
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w400,
                                          color: Constants.kitGradients[30],
                                        ),
                                      ),
                                      SizedBox(
                                        height: screenHeight(context, dividedBy: 70),
                                      ),
                                      Text(
                                        "Swag",
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w400,
                                          color: Constants.kitGradients[30],
                                        ),
                                      ),
                              ],
                            ),
                            ),
                          ],
                        ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 10),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 20,horizontal: 30),
                              // height: screenHeight(context,dividedBy: 5.8),
                              decoration: BoxDecoration(
                                  color: Constants.kitGradients[28],
                                  borderRadius: BorderRadius.circular(15)),
                              child: Container(
                                child: Column(
                                  children: [
                                    Container(
                                      child: Row(
                                        children: [
                                          Container(
                                            width: screenWidth(context,dividedBy: 3),
                                            child: Text(
                                              "Your Orders",
                                              style: TextStyle(
                                                color: Constants.kitGradients[30],
                                                fontWeight: FontWeight.w400,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          GestureDetector(
                                            onTap: () {},
                                            child: Container(
                                              height: screenWidth(context,
                                                  dividedBy: 15),
                                              width: screenWidth(context,
                                                  dividedBy: 15),
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Constants.kitGradients[29]
                                              ),
                                              child:Icon(Icons.arrow_forward_ios,
                                              color: Constants.kitGradients[30],
                                              size: 12,)
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 100),
                                    ),
                                    Divider(
                                      thickness: 0.2,
                                      color: Constants.kitGradients[30].withOpacity(0.5),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 100),
                                    ),
                                    Container(
                                      child: Row(
                                        children: [
                                          Container(
                                            width: screenWidth(context,dividedBy: 3),
                                            child: Text(
                                              "Help",
                                              style: TextStyle(
                                                color: Constants.kitGradients[30],
                                                fontWeight: FontWeight.w400,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          GestureDetector(
                                            onTap: () {
                                            },
                                            child: Container(
                                                height: screenWidth(context,
                                                    dividedBy: 15),
                                                width: screenWidth(context,
                                                    dividedBy: 15),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Constants.kitGradients[29]
                                                ),
                                                child:Icon(Icons.arrow_forward_ios,
                                                  color: Constants.kitGradients[30],
                                                  size: 12,)
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                      screenHeight(context, dividedBy: 100),
                                    ),
                                    Divider(
                                      thickness: 0.2,
                                      color: Constants.kitGradients[30].withOpacity(0.5),
                                    ),
                                    SizedBox(
                                      height:
                                      screenHeight(context, dividedBy: 100),
                                    ),
                                    Container(
                                      child: Row(
                                        children: [
                                          Container(
                                            width: screenWidth(context,dividedBy: 3),
                                            child: Text(
                                              "LogOut",
                                              style: TextStyle(
                                                color: Constants.kitGradients[30],
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          GestureDetector(
                                            onTap: () {
                                              push(context, LoginPage());
                                            },
                                            child: Container(
                                                height: screenWidth(context,
                                                    dividedBy: 15),
                                                width: screenWidth(context,
                                                    dividedBy: 15),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Constants.kitGradients[29]
                                                ),
                                                child:Icon(Icons.arrow_forward_ios,
                                                  color: Constants.kitGradients[30],
                                                  size: 12,)
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Positioned(
                  top: screenHeight(context,dividedBy: 11),
                  left:screenHeight(context,dividedBy: 100),
                  child: Container(
                    width: screenWidth(context, dividedBy: 2.8),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      //border: Border.all(color: Constants.kitGradients[29], width: 10),
                      color: Constants.kitGradients[28],
                    ),
                    child: CircleAvatar(
                      radius: screenWidth(context,dividedBy: 3),
                      backgroundColor: Constants.kitGradients[28],
                      child: CachedNetworkImage(
                        imageUrl: widget.profileImage,
                        imageBuilder: (context, imageProvider) =>
                            Container(
                              width: screenWidth(context, dividedBy: 3),
                              height: screenWidth(context, dividedBy: 3),
                              decoration: BoxDecoration(
                                color: Constants.kitGradients[29],
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover),
                              ),
                            ),
                        placeholder: (context, url) => Center(
                          heightFactor: 1,
                          widthFactor: 1,
                          child: SizedBox(
                            height: 16,
                            width: 16,
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(
                                  Constants.kitGradients[32]),
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                        //fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: CustomBottomBar(
                    currentIndex: 4,
                    onTapHome: () {
                      push(context, HomePage());
                    },
                    onTapProfile: () {},
                    onTapSearch: () {
                      push(context, SearchPage());
                    },
                    onMarketPlace: () {
                      push(context, MarketPlacePage());
                    },
                    onTapUpcoming: () {
                      push(context, UpcomingVideos());
                    },
                  ),
                )
              ],
            )),
      ),
    );
  }
}
