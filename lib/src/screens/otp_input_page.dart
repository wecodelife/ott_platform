import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/form_field_user.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/screens/bottom_navigation.dart';

class OTPInputPage extends StatefulWidget {
  @override
  _OTPInputPageState createState() => _OTPInputPageState();
}

class _OTPInputPageState extends State<OTPInputPage> {
  TextEditingController otpTextEditingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Constants.kitGradients[24],
            body: SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                      children: [
                    SizedBox(
                height: screenHeight(context, dividedBy: 3),
              ),
                    FormFeildUserDetails(
                      labelText: "Enter your OTP",
                      textEditingController: otpTextEditingController,
                      otp: true,
                      isPassword: false,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    GestureDetector(
                      onTap: () {
                        //push(context, LoginPage());
                      },
                      child:Container(
                        alignment: Alignment.centerRight,
                        child: Text(
                        "Resend OTP",
                        style: TextStyle(
                          color: Constants.kitGradients[29],
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),

                      ),),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                         Container(
                width: screenWidth(context, dividedBy: 2.5),
                  child:RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20),),            

                      color: Constants.kitGradients[29],
                      child: Center(
                        child: Text(
                          "Sign Up",
                          style: TextStyle(
                            color: Constants.kitGradients[30],
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      onPressed: () {
                         push(context, BottomNavigation());
                      },
                    ),),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    GestureDetector(
                      onTap: () {
                        push(context, LoginPage());
                      },
                      child: Text(
                        "Already Have an Account ? Sign In",
                        style: TextStyle(
                          color: Constants.kitGradients[29],
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                    )
                  ])),
            )));
  }
}
