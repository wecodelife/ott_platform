import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/market_place_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/screens/upcoming_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/web_series_tile.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

class MovieDetails extends StatefulWidget {
  final String movieTitle;
  final String subTitle;
  final String description;
  final String rating;
  final String time;
  final String imgUrl;
  final String seriesSub;
  final String videoUrl;
  bool isSeries;
  MovieDetails(
      {this.movieTitle,
      this.seriesSub,
      this.subTitle,
      this.imgUrl,
      this.description,
      this.rating,
      this.time,
      this.isSeries,
      this.videoUrl});
  @override
  _MovieDetailsState createState() => _MovieDetailsState();
}

int thumb = 0;
bool backBtn = false;

class _MovieDetailsState extends State<MovieDetails> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  ChewieController _chewieController;
  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.videoUrl);
    _initializeVideoPlayerFuture = _controller.initialize();
    _chewieController = ChewieController(
      videoPlayerController: _controller,
      allowPlaybackSpeedChanging: false,
      allowFullScreen: true,
    );
    _controller.addListener(() {
      setState(() {
        _chewieController.isPlaying ? backBtn = true : backBtn = false;
      });
    });
  }

  // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    _chewieController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                color: Constants.kitGradients[24],
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          Center(
                            child: FutureBuilder(
                              future: _initializeVideoPlayerFuture,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.done) {
                                  return _chewieController != null
                                      ? AspectRatio(
                                          aspectRatio: _controller
                                              .value.size.aspectRatio,
                                          child: Stack(children: [
                                            Container(
                                                width: screenWidth(context,
                                                    dividedBy: 1),
                                                height: screenWidth(context,
                                                    dividedBy: 1.2),
                                                child: Theme(
                                                    data: Theme.of(context)
                                                        .copyWith(
                                                      dialogBackgroundColor:
                                                          Constants
                                                              .kitGradients[28],
                                                      accentColor: Constants
                                                          .kitGradients[29],
                                                      iconTheme: IconThemeData(
                                                          color: Constants
                                                                  .kitGradients[
                                                              29]),
                                                    ),
                                                    child: Chewie(
                                                        controller:
                                                            _chewieController))),
                                          ]))
                                      : Container();
                                } else {
                                  return Container(
                                    width: screenWidth(context, dividedBy: 1),
                                    height:
                                        screenWidth(context, dividedBy: 1.5),
                                    child: Center(
                                      child: CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(
                                            Constants.kitGradients[32]),
                                        strokeWidth: 2,
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                          // ClipRRect(
                          //   //borderRadius: BorderRadius.circular(8),
                          //   child: CachedNetworkImage(
                          //     fit: BoxFit.fill,
                          //     imageUrl: widget.imgUrl,
                          //     imageBuilder: (context, imageProvider) =>
                          //         Container(
                          //       width: screenWidth(context, dividedBy: 1),
                          //       decoration: BoxDecoration(
                          //         image: DecorationImage(
                          //           image: imageProvider,
                          //           fit: BoxFit.fill,
                          //         ),
                          //       ),
                          //     ),
                          //     placeholder: (context, url) => Center(
                          //       heightFactor: 1,
                          //       widthFactor: 1,
                          //       child: SizedBox(
                          //         height: 16,
                          //         width: 16,
                          //         child: CircularProgressIndicator(
                          //           valueColor: AlwaysStoppedAnimation(
                          //               Constants.kitGradients[32]),
                          //           strokeWidth: 2,
                          //         ),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                          // new Positioned(
                          //   child: Center(
                          //     child: GestureDetector(
                          //         child: Container(
                          //           decoration: BoxDecoration(
                          //             color: Constants.kitGradients[28],
                          //             //color:Colors.transparent,
                          //             shape: BoxShape.circle,
                          //           ),
                          //           child: Icon(Icons.play_arrow_rounded,
                          //               color: Constants.kitGradients[29],
                          //               size: 60),
                          //         )),
                          //   ),
                          // ),
                          backBtn
                              ? Container()
                              : new Positioned(
                                  left: 0,
                                  top: 3,
                                  child: GestureDetector(
                                      onTap: () {
                                        pushAndRemoveUntil(
                                            context, HomePage(), false);
                                      },
                                      child: Container(
                                        height:
                                            screenWidth(context, dividedBy: 8),
                                        decoration: BoxDecoration(
                                          color: Constants.kitGradients[28],
                                          //color:Colors.transparent,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(6),
                                              bottomRight: Radius.circular(6)),
                                        ),
                                        child: Icon(
                                            Icons.arrow_back_ios_rounded,
                                            color: Constants.kitGradients[29],
                                            size: 30),
                                      )),
                                )
                        ],
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        padding: EdgeInsets.symmetric(
                            vertical: screenHeight(context, dividedBy: 30),
                            horizontal: screenWidth(context, dividedBy: 30)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(children: [
                              Text(
                                widget.movieTitle,
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Constants.kitGradients[29],
                                ),
                              ),
                              Spacer(),
                              Row(
                                children: [
                                  Container(
                                    height: screenWidth(context, dividedBy: 8),
                                    width: screenWidth(context, dividedBy: 8),
                                    decoration: BoxDecoration(
                                        color: Constants.kitGradients[28],
                                        //color:Colors.transparent,
                                        shape: BoxShape.circle),
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          if (thumb == 0) {
                                            thumb = 1;
                                          } else if (thumb == 1) {
                                            thumb = 0;
                                          } else if (thumb == 2) {
                                            thumb = 1;
                                          }
                                        });
                                      },
                                      child: Icon(
                                        Icons.thumb_up,
                                        size: 30,
                                        color: thumb == 1
                                            ? Constants.kitGradients[29]
                                            : Constants.kitGradients[24],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: screenWidth(context, dividedBy: 30),
                                  ),
                                  Container(
                                    height: screenWidth(context, dividedBy: 8),
                                    width: screenWidth(context, dividedBy: 8),
                                    decoration: BoxDecoration(
                                        color: Constants.kitGradients[28],
                                        //color:Colors.transparent,
                                        shape: BoxShape.circle),
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          if (thumb == 0) {
                                            thumb = 2;
                                          } else if (thumb == 2) {
                                            thumb = 0;
                                          } else if (thumb == 1) {
                                            thumb = 2;
                                          }
                                        });
                                      },
                                      child: Icon(
                                        Icons.thumb_down,
                                        size: 30,
                                        color: thumb == 2
                                            ? Constants.kitGradients[29]
                                            : Constants.kitGradients[24],
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ]),
                            Text(
                              widget.subTitle,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Constants.kitGradients[30],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                            Text(
                              "DESCRIPTION :",
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                color: Constants.kitGradients[30],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 70),
                            ),
                            Text(
                              widget.description,
                              //textAlign: TextAlign.justify,
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                color: Constants.kitGradients[30],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                            Text(
                              "Rating : " + widget.rating,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Constants.kitGradients[30],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 70),
                            ),
                            Text(
                              "Duration : " + widget.time,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Constants.kitGradients[30],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                          ],
                        ),
                      ),
                      widget.isSeries
                          ? Container(
                              padding: EdgeInsets.symmetric(
                                  //vertical: screenHeight(context, dividedBy: 30),
                                  horizontal:
                                      screenWidth(context, dividedBy: 30)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "EPISODES :",
                                    style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w600,
                                      color: Constants.kitGradients[30],
                                    ),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 50),
                                  ),
                                  Container(
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: 5,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Column(
                                          children: [
                                            WebSeriesTile(
                                              seriesCount: index,
                                              subTitle: widget.seriesSub[index],
                                              seriesImages: widget.imgUrl,
                                              seriesDescription:
                                                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                                            ),
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 50),
                                            ),
                                          ],
                                        );
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 20),
                                  ),
                                ],
                              ))
                          : Container(),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: CustomBottomBar(
                  currentIndex: 0,
                  onTapSearch: () {
                    push(context, SearchPage());
                  },
                  onTapProfile: () {
                    push(
                        context,
                        ProfilePage(
                            username: "Brandon Jake",
                            email: "Brandon@example.com",
                            profileImage:
                                "https://images.unsplash.com/photo-1597466765990-64ad1c35dafc"));
                  },
                  onMarketPlace: () {
                    push(context, MarketPlacePage());
                  },
                  onTapUpcoming: () {
                    push(context, UpcomingVideos());
                  },
                  onTapHome: () {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
