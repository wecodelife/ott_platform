import 'package:app_template/src/screens/data_page.dart';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/item_cart_page.dart';
import 'package:app_template/src/screens/item_details_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/trending_list_page.dart';
import 'package:app_template/src/screens/upcoming_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:app_template/src/widgets/category_tile.dart';
import 'package:app_template/src/widgets/heading_tile.dart';
// import 'package:app_template/src/widgets/heading_tile.dart';
import 'package:app_template/src/widgets/image_slider.dart';
import 'package:app_template/src/widgets/market_place_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MarketPlacePage extends StatefulWidget {
  @override
  _MarketPlacePageState createState() => _MarketPlacePageState();
}

class _MarketPlacePageState extends State<MarketPlacePage> {
  List<String> images = [
    "https://images.unsplash.com/photo-1611232099906-dc95961260a4?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDR8dG93SlpGc2twR2d8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    "https://images.unsplash.com/photo-1607973259090-0f86a2225ca7?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDN8dG93SlpGc2twR2d8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1611232099906-dc95961260a4?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDR8dG93SlpGc2twR2d8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    "https://images.unsplash.com/photo-1607973259090-0f86a2225ca7?ixid=MnwxMjA3fDB8MHx0b3BpYy1mZWVkfDN8dG93SlpGc2twR2d8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];

  List<String> clothing = [
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1572804013427-4d7ca7268217?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGRyZXNzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1488371934083-edb7857977df?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8bWVufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];
  List<String> men = [
    "https://images.unsplash.com/photo-1562157873-818bc0726f68?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1467043237213-65f2da53396f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1488371934083-edb7857977df?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8bWVufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];
  List<String> women = [
    "https://images.unsplash.com/photo-1558769132-cb1aea458c5e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1556905055-8f358a7a47b2?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1551232864-3f0890e580d9?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];
  List<String> kids = [
    "https://images.unsplash.com/photo-1571210862729-78a52d3779a2?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8a2lkc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1531325082793-ca7c9db6a4c1?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8a2lkc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1503919545889-aef636e10ad4?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGtpZHN8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];
  List<String> categories = ["Men", "Women", "Kids", "All"];
  List<String> type = ["Ethnic", "Casual", "Sports", "All"];
  List<String> features = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
  ];

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        backgroundColor: Constants.kitGradients[31],
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                //height: screenHeight(context, dividedBy: 1),
                width: screenWidth(context, dividedBy: 1),
                color: Constants.kitGradients[24],
                child: Column(
                  children: [
                    Container(
                      height: screenHeight(context, dividedBy: 2.4),
                      width: screenWidth(context, dividedBy: 1),
                      // color: Constants.kitGradients[24],
                      child: CustomImageSlider(sliderImages: clothing),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    Container(
                      //height: screenHeight(context, dividedBy: 4.5),
                      margin: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 40),
                      ),
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        vertical: screenHeight(context, dividedBy: 50),
                        horizontal: screenWidth(context, dividedBy: 60),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Constants.kitGradients[28],
                      ),
                      child: Column(
                        children: [
                          SizedBox(
                              height: screenHeight(context, dividedBy: 90)),
                          HeadingTile(
                            heading: "Fashion",
                            onPressed: () {
                              push(
                                  context,
                                  TrendingList(
                                    pageTitle: "Fashion",
                                    selection: false,
                                  ));
                            },
                          ),
                          SizedBox(
                              height: screenHeight(context, dividedBy: 70)),
                          Container(
                            width: screenWidth(context, dividedBy: 1),
                            height: screenHeight(context, dividedBy: 5),
                            child: ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: clothing.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Row(children: [
                                  MarketPlaceCard(
                                    imgUrl: clothing[index],
                                    catType: type[index],
                                    circle: true,
                                    itemPressed: () {
                                      push(
                                        context,
                                        ItemDetailsPage(
                                          itemName: "Casual",
                                          itemImage:
                                              "https://images.unsplash.com/photo-1467043237213-65f2da53396f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
                                          itemPrice: "670",
                                          itemRating: "8.1",
                                          features: features,
                                        ),
                                      );
                                    },
                                  ),
                                  SizedBox(
                                    width: screenWidth(context, dividedBy: 30),
                                  )
                                ]);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: items.map((item) {
                        return CategoryListTile(
                          itemtype: item['type'],
                          images: item['images'],
                          categoryType: item['category'],
                          seeMore: () {},
                          itemPressed: () {
                            push(
                              context,
                              ItemDetailsPage(
                                itemName: "Casual",
                                itemImage:
                                    "https://images.unsplash.com/photo-1467043237213-65f2da53396f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
                                itemPrice: "670",
                                itemRating: "8.1",
                                features: features,
                              ),
                            );
                          },
                        );
                      }).toList(),
                    ),
                    SizedBox(height: screenHeight(context, dividedBy: 10)),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: CustomBottomBar(
                currentIndex: 3,
                onTapSearch: () {
                  //push(context, MarketPlacePage());
                },
                onTapProfile: () {
                  push(
                      context,
                      ProfilePage(
                          username: "Brandon Jake",
                          email: "Brandon@example.com",
                          profileImage:
                              "https://images.unsplash.com/photo-1597466765990-64ad1c35dafc"));
                },
                onTapUpcoming: () {
                  push(context, UpcomingVideos());
                },
                onTapHome: () {
                  push(context, HomePage());
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
