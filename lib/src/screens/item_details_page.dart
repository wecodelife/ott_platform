import 'package:app_template/src/screens/item_cart_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/drop_down_list.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ItemDetailsPage extends StatefulWidget {
  final String itemName;
  final String itemImage;
  final String itemPrice;
  final String itemRating;
  final List<String> features;
  ItemDetailsPage(
      {this.itemName,
      this.itemImage,
      this.itemPrice,
      this.itemRating,
      this.features});
  @override
  _ItemDetailsPageState createState() => _ItemDetailsPageState();
}

List<String> size = ["S", "M", "L", "XL", "XXL"];
int itemCount = 0;
String sizeType;

class _ItemDetailsPageState extends State<ItemDetailsPage> {
  int count = 1;
  int bcount = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[24],
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[24],
          leading: GestureDetector(
            onTap: () {
              pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Constants.kitGradients[27],
            ),
          ),
          title: Container(
            child: Row(
              children: [
                Text(
                  widget.itemName,
                  style: TextStyle(
                    //fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Constants.kitGradients[27],
                  ),
                ),
                Spacer(),
                GestureDetector(
                    onTap: () {
                      push(
                          context,
                          ItemCartPage(
                              itemPrice: widget.itemPrice,
                              itemImage: widget.itemImage,
                              itemName: widget.itemName));
                    },
                    child: Icon(
                      Icons.shopping_cart_outlined,
                      color: Constants.kitGradients[27],
                    ))
              ],
            ),
          ),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30)),
                child: Column(
                  children: [
                    CachedNetworkImage(
                      fit: BoxFit.cover,
                      placeholder: (context, url) => Center(
                        heightFactor: 1,
                        widthFactor: 1,
                        child: SizedBox(
                          height: 16,
                          width: 16,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(
                                Constants.kitGradients[29]),
                            strokeWidth: 2,
                          ),
                        ),
                      ),
                      imageUrl: widget.itemImage,
                      imageBuilder: (context, imageProvider) => Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenWidth(context, dividedBy: 1.5),
                              decoration: BoxDecoration(
                                color: Constants.kitGradients[29],
                                borderRadius: BorderRadius.circular(7),
                                //shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.fill),
                              ),
                            ),
                            SizedBox(
                                height: screenHeight(context, dividedBy: 30)),
                            Row(
                              children: [
                                Text(
                                  widget.itemName,
                                  // textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: Constants.kitGradients[29],
                                    fontWeight: FontWeight.bold,
                                    fontSize: 24,
                                  ),
                                ),
                                // SizedBox(
                                //     width: screenWidth(context, dividedBy: 25)),
                                // Container(
                                //   width: screenWidth(context, dividedBy: 10),
                                //   height: screenWidth(context, dividedBy: 16),
                                //   decoration: BoxDecoration(
                                //     color: Constants.kitGradients[29],
                                //     borderRadius: BorderRadius.circular(5),
                                //   ),
                                //   alignment: Alignment.center,
                                //   child: Text(
                                //     widget.itemRating,
                                //     // textAlign: TextAlign.left,
                                //     style: TextStyle(
                                //       color: Constants.kitGradients[27],
                                //       fontWeight: FontWeight.bold,
                                //       fontSize: 17,
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                            Text(
                              "₹ " + widget.itemPrice,
                              // textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Constants.kitGradients[27],
                                fontWeight: FontWeight.w500,
                                fontSize: 20,
                              ),
                            ),
                            Divider(
                              color: Constants.kitGradients[29],
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 50),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        "AVAILABLE SIZES:",
                                        style: TextStyle(
                                          color: Constants.kitGradients[27],
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18,
                                        ),
                                      ),
                                      SizedBox(
                                          width: screenWidth(context,
                                              dividedBy: 30)),
                                      DropDownList(
                                        title: "Select",
                                        dropDownList: size,
                                        dropDownValue: sizeType,
                                        onClicked: (value) {
                                          setState(() {
                                            sizeType = value;
                                          });
                                        },
                                      ),
                                    ],
                                  ),

                                  // Container(
                                  //   //width: screenWidth(context, dividedBy: 1),
                                  //   //  height: screenHeight(context,
                                  //   //           dividedBy: 3),
                                  //   child: GridView.builder(
                                  //     shrinkWrap: true,
                                  //     physics: NeverScrollableScrollPhysics(),
                                  //     gridDelegate:
                                  //         SliverGridDelegateWithFixedCrossAxisCount(
                                  //       crossAxisCount: 4,
                                  //       mainAxisSpacing: 5,
                                  //       crossAxisSpacing: 5,
                                  //       childAspectRatio:
                                  //           screenWidth(context, dividedBy: 1) /
                                  //               (screenHeight(context,
                                  //                       dividedBy: 1) /
                                  //                   4),
                                  //     ),
                                  //     itemCount: size.length,
                                  //     itemBuilder:
                                  //         (BuildContext context, int index) {
                                  //       return Container(
                                  //         // width: 10,
                                  //         // height: 10,
                                  //         alignment: Alignment.center,
                                  //         decoration: BoxDecoration(
                                  //           color: Constants.kitGradients[29],
                                  //           borderRadius:
                                  //               BorderRadius.circular(5),
                                  //         ),
                                  //         child: Text(
                                  //           size[index],
                                  //           style: TextStyle(
                                  //             color: Constants.kitGradients[27],
                                  //             fontWeight: FontWeight.w500,
                                  //             fontSize: 16,
                                  //           ),
                                  //         ),
                                  //       );
                                  //     },
                                  //   ),
                                  // ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 30),
                                  ),
                                  Container(
                                      child: Row(
                                    children: [
                                      Text(
                                        "QUANTITY YOU NEED:",
                                        style: TextStyle(
                                          color: Constants.kitGradients[27],
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                        ),
                                      ),
                                      SizedBox(
                                        width:
                                            screenWidth(context, dividedBy: 30),
                                      ),
                                      Container(
                                          height: screenHeight(context,
                                              dividedBy: 10),
                                          child: Column(
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    itemCount++;
                                                  });
                                                },
                                                child: Icon(
                                                  Icons.arrow_drop_up,
                                                  color: Constants
                                                      .kitGradients[27],
                                                ),
                                              ),
                                              Text(
                                                itemCount.toString(),
                                                style: TextStyle(
                                                  color: Constants
                                                      .kitGradients[29],
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 17,
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    if (itemCount > 0)
                                                      itemCount--;
                                                  });
                                                },
                                                child: Icon(
                                                  Icons.arrow_drop_down,
                                                  color: Constants
                                                      .kitGradients[27],
                                                ),
                                              ),
                                            ],
                                          ))
                                    ],
                                  )),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 10),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: screenHeight(context, dividedBy: 15),
                color: Constants.kitGradients[24],
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30),
                ),
                margin: EdgeInsets.symmetric(
                  //horizontal: screenWidth(context, dividedBy: 30),
                  vertical: screenHeight(context, dividedBy: 80),
                ),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          count == 1
                              ? setState(() {
                                  count = 2;
                                })
                              : setState(() {
                                  count = 1;
                                });

                          push(
                              context,
                              itemCount > 0 && sizeType != "Select"
                                  ? ItemCartPage(
                                      itemPrice: widget.itemPrice,
                                      itemImage: widget.itemImage,
                                      itemName: widget.itemName)
                                  : showDialog(
                                      context: context,
                                      builder: (context) {
                                        Future.delayed(Duration(seconds: 2),
                                            () {
                                          Navigator.of(context).pop(true);
                                        });
                                        return AlertDialog(
                                          content: Text(
                                              'Please select the size and quantity to be purchased'),
                                        );
                                      }));
                        },
                        child: Container(
                          width: screenWidth(context, dividedBy: 2.2),
                          height: screenHeight(context, dividedBy: 15),
                          decoration: BoxDecoration(
                            color: count == 2
                                ? Constants.kitGradients[29]
                                : Colors.transparent,
                            border: Border.all(
                              color: Constants.kitGradients[29],
                            ),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            "ADD TO CART",
                            style: TextStyle(
                              color: Constants.kitGradients[27],
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ),
                      Spacer(),
                      GestureDetector(
                        onTap: () {
                          bcount == 1
                              ? setState(() {
                                  bcount = 2;
                                })
                              : setState(() {
                                  bcount = 1;
                                });
                        },
                        child: Container(
                          width: screenWidth(context, dividedBy: 2.2),
                          height: screenHeight(context, dividedBy: 15),
                          decoration: BoxDecoration(
                            color: Constants.kitGradients[29],
                            border: Border.all(
                              color: Constants.kitGradients[29],
                            ),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            "BUY",
                            style: TextStyle(
                              color: Constants.kitGradients[27],
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      )
                    ]),
              ),
            )
          ],
        ));
  }
}
