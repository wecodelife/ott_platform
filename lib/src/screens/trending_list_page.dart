import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/item_details_page.dart';
import 'package:app_template/src/screens/market_place_page.dart';
import 'package:app_template/src/screens/movie_details_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/search_page.dart';
import 'package:app_template/src/screens/upcoming_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/bottom_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TrendingList extends StatefulWidget {
  final String pageTitle;
  final bool selection;
  TrendingList({this.pageTitle, this.selection});
  @override
  _TrendingListState createState() => _TrendingListState();
}

class _TrendingListState extends State<TrendingList> {
  List<String> images = [
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1572804013427-4d7ca7268217?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGRyZXNzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1488371934083-edb7857977df?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8bWVufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1572804013427-4d7ca7268217?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGRyZXNzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1488371934083-edb7857977df?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8bWVufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];

  List<String> clothing = [
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1572804013427-4d7ca7268217?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGRyZXNzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1488371934083-edb7857977df?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8bWVufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];
  List<String> men = [
    "https://images.unsplash.com/photo-1562157873-818bc0726f68?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1467043237213-65f2da53396f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1488371934083-edb7857977df?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8bWVufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];
  List<String> women = [
    "https://images.unsplash.com/photo-1558769132-cb1aea458c5e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1556905055-8f358a7a47b2?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1551232864-3f0890e580d9?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];
  List<String> kids = [
    "https://images.unsplash.com/photo-1571210862729-78a52d3779a2?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8a2lkc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1531325082793-ca7c9db6a4c1?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8a2lkc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1503919545889-aef636e10ad4?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTN8fGtpZHN8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
    "https://images.unsplash.com/photo-1595777457583-95e059d581b8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8ZHJlc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
  ];
  List<String> features = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
  ];
  final List<String> imgs = [
    "https://cdn.dribbble.com/users/3408570/screenshots/10746910/media/25b3567e37c225bad3dfb92133e1dc36.jpg?compress=1&resize=800x600",
    "https://www.joblo.com/assets/images/joblo/news/2021/03/mortal-kombat-2021-poster-group.jpg",
    "https://hdfreewallpaper.net/wp-content/uploads/2015/03/movies-wallpaper.jpg",
    "https://cdn.dribbble.com/users/3408570/screenshots/10746910/media/25b3567e37c225bad3dfb92133e1dc36.jpg?compress=1&resize=800x600",
    "https://www.joblo.com/assets/images/joblo/news/2021/03/mortal-kombat-2021-poster-group.jpg",
    "https://hdfreewallpaper.net/wp-content/uploads/2015/03/movies-wallpaper.jpg",
    "https://www.joblo.com/assets/images/joblo/news/2021/03/mortal-kombat-2021-poster-group.jpg",
    "https://cdn.dribbble.com/users/3408570/screenshots/10746910/media/25b3567e37c225bad3dfb92133e1dc36.jpg?compress=1&resize=800x600",
  ];
  List<String> videos = [
    "https://www.ibrahimabah.com/ibfilms/Harry.Potter-The.Ultimate.Collection.%20I%20-%20VIII%20.%202001-2011.1080p.Bluray.x264.anoXmous/Harry%20Potter%208/Harry.Potter.And.The.Deathly.Hallows.Part.2.2011.1080p.Bluray.x264.anoXmous_.mp4",
    "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
    "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4",
    "https://www.ibrahimabah.com/ibfilms/Harry.Potter-The.Ultimate.Collection.%20I%20-%20VIII%20.%202001-2011.1080p.Bluray.x264.anoXmous/Harry%20Potter%208/Harry.Potter.And.The.Deathly.Hallows.Part.2.2011.1080p.Bluray.x264.anoXmous_.mp4",
    "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
    "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4",
    "https://www.ibrahimabah.com/ibfilms/Harry.Potter-The.Ultimate.Collection.%20I%20-%20VIII%20.%202001-2011.1080p.Bluray.x264.anoXmous/Harry%20Potter%208/Harry.Potter.And.The.Deathly.Hallows.Part.2.2011.1080p.Bluray.x264.anoXmous_.mp4",
    "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
  ];

  final List<String> titles = [
    "BATMAN",
    "Mortal Kombat",
    "RockStar",
    "BATMAN",
    "Mortal Kombat",
    "RockStar",
    "BATMAN",
    "Mortal Kombat",
  ];
  final List<String> movieType = [
    "Action",
    "Action",
    "Drama",
    "Action",
    "Action",
    "Drama",
    "Action",
    "Action",
  ];
  final List<String> rating = [
    "8.2",
    "7.4",
    "6.4",
    "8.2",
    "7.4",
    "6.4",
    "8.2",
    "7.4",
  ];
  final List<String> duration = [
    "2 hr 20 min",
    "1 hr 19 min",
    "2 hr 3 min",
    "2 hr 20 min",
    "1 hr 19 min",
    "2 hr 3 min",
    "2 hr 20 min",
    "1 hr 19 min",
  ];

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Constants.kitGradients[24],
        ),
        child: SafeArea(
          child: Scaffold(
              backgroundColor: Constants.kitGradients[24],
              appBar: AppBar(
                backgroundColor: Constants.kitGradients[24],
                leading: Container(),
                actions: [
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 10),
                    color: Constants.kitGradients[24],
                    padding: EdgeInsets.symmetric(
                        //vertical: screenHeight(context, dividedBy: 90)
                        ),
                    child: Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 20),
                        ),
                        Text(
                          widget.pageTitle,
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Constants.kitGradients[30],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              body: Container(
                  child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        widget.selection
                            ? Container(
                                width: screenWidth(context, dividedBy: 1),
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      screenWidth(context, dividedBy: 30),
                                  vertical:
                                      screenHeight(context, dividedBy: 50),
                                ),
                                color: Constants.kitGradients[24],
                                child: GridView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    crossAxisSpacing: 13.0,
                                    mainAxisSpacing: 13.0,
                                  ),
                                  itemCount: imgs.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                        child: GestureDetector(
                                      onTap: () {
                                        push(
                                            context,
                                            MovieDetails(
                                              movieTitle: titles[index],
                                              subTitle: movieType[index],
                                              imgUrl: imgs[index],
                                              description:
                                                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                                              rating: rating[index],
                                              time: duration[index],
                                              isSeries: false,
                                              videoUrl: videos[index],
                                            ));
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(5),
                                        child: CachedNetworkImage(
                                          width: screenWidth(context,
                                              dividedBy: 4),
                                          height: screenWidth(context,
                                              dividedBy: 4),
                                          fit: BoxFit.fill,
                                          placeholder: (context, url) => Center(
                                            heightFactor: 1,
                                            widthFactor: 1,
                                            child: SizedBox(
                                              height: 16,
                                              width: 16,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation(
                                                        Constants
                                                            .kitGradients[32]),
                                                strokeWidth: 2,
                                              ),
                                            ),
                                          ),
                                          imageUrl: imgs[index],
                                        ),
                                      ),
                                    ));
                                  },
                                ),
                              )
                            : Container(
                                width: screenWidth(context, dividedBy: 1),
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      screenWidth(context, dividedBy: 30),
                                  vertical:
                                      screenHeight(context, dividedBy: 50),
                                ),
                                color: Constants.kitGradients[24],
                                child: GridView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    crossAxisSpacing: 13.0,
                                    mainAxisSpacing: 13.0,
                                  ),
                                  itemCount: images.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                        child: GestureDetector(
                                      onTap: () {
                                        push(
                                          context,
                                          ItemDetailsPage(
                                            itemName: "Casual",
                                            itemImage:
                                                "https://images.unsplash.com/photo-1467043237213-65f2da53396f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8Y2xvdGhlc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60",
                                            itemPrice: "670",
                                            itemRating: "8.1",
                                            features: features,
                                          ),
                                        );
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(5),
                                        child: CachedNetworkImage(
                                          width: screenWidth(context,
                                              dividedBy: 4),
                                          height: screenWidth(context,
                                              dividedBy: 4),
                                          fit: BoxFit.fill,
                                          placeholder: (context, url) => Center(
                                            heightFactor: 1,
                                            widthFactor: 1,
                                            child: SizedBox(
                                              height: 16,
                                              width: 16,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation(
                                                        Constants
                                                            .kitGradients[32]),
                                                strokeWidth: 2,
                                              ),
                                            ),
                                          ),
                                          imageUrl: images[index],
                                        ),
                                      ),
                                    ));
                                  },
                                ),
                              ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 10),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: CustomBottomBar(
                      currentIndex: 0,
                      onTapSearch: () {
                        push(context, SearchPage());
                      },
                      onTapUpcoming: () {
                        push(context, UpcomingVideos());
                      },
                      onMarketPlace: () {
                        push(context, MarketPlacePage());
                      },
                      onTapProfile: () {
                        push(
                            context,
                            ProfilePage(
                                username: "Brandon Jake",
                                email: "Brandon@example.com",
                                profileImage:
                                    "https://images.unsplash.com/photo-1597466765990-64ad1c35dafc"));
                      },
                      onTapHome: () {
                        pushAndRemoveUntil(context, HomePage(), false);
                      },
                    ),
                  ),
                ],
              ))),
        ));
  }
}
